/*
    NPM faker exercise
    Create a new directory named MyShop
    Add a file named listProducts.js
    Install the fake package
    Figure out how to print 10 random product names and prices
    Run the file and make sure it works
*/


var faker = require("faker");

console.log("=======================");
console.log("Welcome To My Shop!!");
console.log("=======================");

for (var i = 0; i < 10; i++){
    console.log(faker.fake("{{commerce.productName}}, {{commerce.price}}"));
}
