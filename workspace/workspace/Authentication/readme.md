#Authentication

##Intro to Auth
* What tools are we using?
*   Passport
*   Passport Local
*   Passport Local Mongoose
* Walk through auth flow
* Discuss sections
*   Express-Session

#Auth Code Along Part 1
* Set up folder structure
* Install needed packages
* Add root route and template
* Add secret route and template

#Auth Code Along Part 2
* Create User model
* Configure passport