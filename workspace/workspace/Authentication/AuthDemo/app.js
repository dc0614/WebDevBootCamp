var express = require("express");

var app = express();
var mongoose    = require("mongoose");
mongoose.connect("mongodb://localhost/yelp_camp_v3");

app.set("view engine", "ejs");

app.get("/", function(req, res){
    res.render("home");
});


app.get("/secret", function(req, res){
    res.render("secret");
});


//Start Server
app.listen(process.env.PORT, process.env.IP, function(){
    console.log("Server Has Started......");
});
