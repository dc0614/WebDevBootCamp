var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/blog_demo");


// POST - title, content
var postSchema = new mongoose.Schema({
    title: String,
    content: String
});
var Post = mongoose.model("Post", postSchema);


//USER - email, name
var userSchema = new mongoose.Schema({
    email: String,
    name: String,
    posts: [postSchema]
});
var User = mongoose.model("User", userSchema);


User.findOne({name: "Mickey Mouse"}, function(err, user){
    if (err){
        console.log(err);
    } else{
        user.posts.push({
            title: "3 Things I Really Love",
            content: "Minnie. Pluto. Goofy"
        });
        user.save(function(err, user){
           if(err){
               console.log(err);
           } else {
               console.log(user);
           }
        });
    }
});

/*
var newUser = new User({
    email: "mickeym@mouseuniversity.edu",
    name: "Mickey Mouse"
});


newUser.posts.push({
    title: "Uh, Hiya There Pal!",
    content: "I just wanted to say Hello!"
});

newUser.save(function(err, user){
    if(err){
        console.log(err);
    } else {
        console.log(user);
    }
})

/*
var newUser = new User({
    email: "charlie@brown.edu",
    name: "Charlie Brown"
});
newUser.save(function(err, user){
    if(err){
        console.log(err);
    } else {
        console.log(user);
    }
})
*/


/*
var newPost = new Post({
    title: "Reflections on Apples",
    content: "They are delicious"
});
newPost.save(function(err, post){
    if(err){
        console.log(err);
    } else {
        console.log(post);
    }
});
*/