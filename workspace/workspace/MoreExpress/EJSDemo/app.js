var express = require("express");
var app = express();

app.use(express.static("public"));
app.set("view engine", "ejs");

app.get("/", function(req, res){
    res.render("home");
});


app.get("/fallinlovewith/:something", function(req, res){
   var something = req.params.something.toLowerCase(); 
   res.render("love", {thing: something});
});


app.get("/posts", function(req, res) {
    var posts = [
        {title: "Post 1", author: "Susy"},
        {title: "My adorable pet dragon", author: "DC"},
        {title: "Can you believe this pomsky?", author: "DC"},
    ];
    
    res.render("posts", {posts: posts});
});

app.listen(process.env.PORT, process.env.IP, function(){
    console.log("Server is alive!!!");
});