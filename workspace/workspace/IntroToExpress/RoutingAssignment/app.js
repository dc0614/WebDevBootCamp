var express = require("express");
var app = express();


/*
    root route "/" 
    Displays default welcome message

*/
app.get("/", function(req, res){
    res.send("Hi there, welcome to my assignment!");
})



/*
    "/speak" route
    prints the sound of the corresponding :/animalName
*/
app.get("/speak/:animalName", function(req, res) {
   var animalSounds = {
       pig: "Oink",
       cow: "Moo",
       dog: "Woof Woof",
       cat: "Meow", 
       owl: "Whoo"
   }
   
   var animal = req.params.animalName.toLowerCase();
   var sound = animalSounds[animal];
   
   res.send("The " + animal + " says '" + sound + "'");
   
});



/*
    "/repeat" route 
    repeat the users /:word of choice the /:number of times the user specified
    
*/
app.get("/repeat/:word/:number", function(req, res) {
   var userWord = req.params.word;
   var userNum = Number(req.params.number);
   var finalStr = "";
   
   for(var i = 0; i < userNum; i++){
       finalStr += userWord + " ";
   }
   
   res.send(finalStr);
});



// return for any route that is not defined
app.get("*", function(req, res){
   res.send("Sorry page not found...What are you doing with your life?"); 
});


//Tell Express to listen for requests (start server)
app.listen(process.env.PORT, process.env.IP, function(){
    console.log("Server has started!");
});