var express = require("express");
var app = express();

// "/" => "Hi there!"
app.get("/", function(req, res){
    res.send("Hi there!");
});


// "/bye" => "Goodbye!"
app.get("/bye", function(req, res){
    res.send("Goodbye!");
});


// "/dog" => "WOOF!"
app.get("/dog", function(req, res){
    res.send("WOOF!");
    console.log("Someone made a request to /dog");
});

//
app.get("/r/:subredditName", function(req, res) {
    var subreddit = req.params.subredditName;
    res.send("WELCOME TO THE " + subreddit.toUpperCase() + " SUBREDDIT!");
});

app.get("/r/:subredditName/comments/:id/:title", function(req, res) {
    res.send("WELCOME to the COMMENTS PAGE!");
});

//"*" => return a message for any route that is not defined
app.get("*", function(req, res) {
    res.send("You are a STAR!");
});


//Tell Express to listen for requests (start server)
app.listen(process.env.PORT, process.env.IP, function(){
    console.log("Server has started!");
});