/*
    Node Average Excercise
    Create a new file grader.js
    In the file define a function named average
    It should take a single parameter: an array of test scores(all numbers)
    It should return the average score in the array, rounded to the nearest whole number
*/



function average(scores){
    //keep track of sum
    var sum = 0;
    
    //loop through and gather sum of scores array
    for(var i = 0; i < scores.length; i++){
        sum += scores[i];
    }
    
    //declare and initialize avsum with the average of all the scores
    var avsum = sum / scores.length;
    
    //round and return the average 
    return Math.round(avsum);
}


var scores = [90, 98, 89, 100, 100, 86, 94];
console.log(average(scores));


var scores = [40, 65, 77, 82, 80, 54, 73, 63, 95, 49];
console.log(average(scores));