/*
    Node Echo Excerise
    Create a file named "echo.js" from the cmd line
    Write a function named echo that takes two arguments, a string and a number 
    The function will print the string x number of times
    Use the following test cases below
    echo("Echo!!! ", 10);
    echo("Tater Tots ", 3);
*/

function echo(str, num) {
    //output displayed to the console
    //used the repeat method to print the given string x number of times
    console.log(str.repeat(num));
}

echo("Echo!!! ", 10);
echo("Tater Tots ", 3);