var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/lizard_app");

//pattern, sometype of structure
var lizSchema = new mongoose.Schema({
    name: String,
    age: Number,
    breed: String
});

//provides all the methods, name of the singular version in her database
var Lizard = mongoose.model("Lizard", lizSchema);

/*
//adding a new cat to the db
var breezy = new Lizard({
    name: "Georgia",
    age: 5,
    breed: "Gecko"
});

breezy.save(function(err, lizard){
    if(err){
        console.log("Something went wrong!");
    }   else {
        console.log("We just saved a lizard to the db");
        console.log(lizard);
    }
})
*/

Lizard.create({
    name: "Gator",
    age: 3,
    breed: "Anole"
}, function(err, lizard){
    if(err){
        console.log(err);
    } else {
        console.log(lizard);
    }
});

//retrieve all cats from the db and console.log each one

Lizard.find({}, function(err, lizards){
    if(err){
        console.log("Oh No, error!");
        console.log(err);
    } else {
        console.log("All the lizards!");
        console.log(lizards);
    }
});