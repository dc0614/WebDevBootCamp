#Databases

##Intro to Databases


#Intro to MongoDB
* What is MongoDB?
* Why are we using it?
* Let's Install it!

MEAN Stack 
Mongo, Express, Angular and Node

#Our First Mongo Commands
* mongod
* mongo
* help
* show dbs
* use 

#CRUD
* insert
* find
* update
* remove

#Mongoose
* What is Mongoose?
* Why are we using it?
* Interact with a Mongo Database using Mongoose


