var mongoose = require("mongoose");
var Campground = require("./models/campground");
var Comment = require("./models/comment");


var data = [
    {
        name: "Cloud's Rest",
        image: "http://static.panoramio.com/photos/large/44103092.jpg",
        description: "Spicy jalapeno chuck kevin frankfurter drumstick hamburger tenderloin ribeye swine short loin filet mignon boudin ham t-bone fatback brisket. Swine cow ground round brisket spare ribs hamburger pancetta pastrami picanha shoulder filet mignon shankle. Landjaeger turducken strip steak ball tip. Burgdoggen alcatra fatback drumstick. Ground round pig drumstick tongue. Prosciutto frankfurter cow landjaeger short ribs ground round. Strip steak short loin corned beef, kevin cow sausage ribeye burgdoggen."
    },
    {
        name: "Love's Space",
        image: "https://images.fineartamerica.com/images/artworkimages/mediumlarge/1/tropical-twilight-ii-charmaine-zoe.jpg",
        description: "Spicy jalapeno chuck kevin frankfurter drumstick hamburger tenderloin ribeye swine short loin filet mignon boudin ham t-bone fatback brisket. Swine cow ground round brisket spare ribs hamburger pancetta pastrami picanha shoulder filet mignon shankle. Landjaeger turducken strip steak ball tip. Burgdoggen alcatra fatback drumstick. Ground round pig drumstick tongue. Prosciutto frankfurter cow landjaeger short ribs ground round. Strip steak short loin corned beef, kevin cow sausage ribeye burgdoggen."
    },
    {
        name: "Hunter's Cove",
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnFlqIsrZR47VJ8pqb_lZsJ7Xo4TIonw3HbZOHTcOuufYtLWVB",
        description: "Spicy jalapeno chuck kevin frankfurter drumstick hamburger tenderloin ribeye swine short loin filet mignon boudin ham t-bone fatback brisket. Swine cow ground round brisket spare ribs hamburger pancetta pastrami picanha shoulder filet mignon shankle. Landjaeger turducken strip steak ball tip. Burgdoggen alcatra fatback drumstick. Ground round pig drumstick tongue. Prosciutto frankfurter cow landjaeger short ribs ground round. Strip steak short loin corned beef, kevin cow sausage ribeye burgdoggen."
    }
    
]


function seedDB(){
    //Remove all campgrounds
    Campground.remove({}, function(err){
        if(err){
            console.log(err);
        } else {
            console.log("Successfully removed campgrounds!");
            
             //Add a few camprounds
            data.forEach(function(seed){
                Campground.create(seed, function(err, campground){
                    if(err){
                        console.log(err);
                    } else {
                        console.log("added a campground!");
                        //Add a few comments
                        Comment.create(
                            {
                                text: "I had an amazing time, so chill!",
                                author: "Bart"
                            }, function(err, comment){
                                if(err){
                                    console.log(err);
                                } else {
                                    campground.comments.push(comment);
                                    campground.save();
                                    console.log("Created new comment");
                                }
                        });
                    }
                })
            });
        }
    });

}

module.exports = seedDB;
