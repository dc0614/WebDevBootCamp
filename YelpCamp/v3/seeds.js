var mongoose = require("mongoose");
var Campground = require("./models/campground");
var Comment = require("./models/comment");


var data = [
    {
        name: "Cloud's Rest",
        image: "http://static.panoramio.com/photos/large/44103092.jpg",
        description: "This place has tons of land, great during hunting or fishing season"
    },
    {
        name: "Love's Space",
        image: "https://images.fineartamerica.com/images/artworkimages/mediumlarge/1/tropical-twilight-ii-charmaine-zoe.jpg",
        description: "Great space for a romantic getaway"
    },
    {
        name: "Hunter's Cove",
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnFlqIsrZR47VJ8pqb_lZsJ7Xo4TIonw3HbZOHTcOuufYtLWVB",
        description: "Always a relaxing adventure, great for nature lovers!"
    }
    
]


function seedDB(){
    //Remove all campgrounds
    Campground.remove({}, function(err){
        if(err){
            console.log(err);
        } else {
            console.log("Successfully removed campgrounds!");
            
             //Add a few camprounds
            data.forEach(function(seed){
                Campground.create(seed, function(err, campground){
                    if(err){
                        console.log(err);
                    } else {
                        console.log("added a campground!");
                        //Add a few comments
                        Comment.create(
                            {
                                text: "I had an amazing time, so chill!",
                                author: "Bart"
                            }, function(err, comment){
                                if(err){
                                    console.log(err);
                                } else {
                                    campground.comments.push(comment);
                                    campground.save();
                                    console.log("Created new comment");
                                }
                        });
                    }
                })
            });
        }
    });

}

module.exports = seedDB;
